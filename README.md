# 2D Path Finding Unity

A C#/Unity implementation of the 2D pathfinding and smoothing algorithm described here
 https://www.shdon.com/blog/2022/03/30/pathfinding-in-point-and-click-adventure-games

Also implements an algorithm from here
http://ericw.ca/notes/bresenhams-line-algorithm-in-csharp.html

Note that while written for use in Unity, this script has very little dependence on Unity libraries. Only a couple of minor changes are needed to adapt the code what ever coordinate system you are using and this is described in the code comments.

