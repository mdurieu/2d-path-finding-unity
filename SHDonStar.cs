using System;
using System.Collections.Generic;
using UnityEngine;

public static class SHDonStar
{
    /* This class is an implementation of the path finding algorithm described here https://www.shdon.com/blog/2022/03/30/pathfinding-in-point-and-click-adventure-games
     * This is written in C# and to be used with the Unity engine, though is is only dependent on the Vector2 class from Unity (which holds float coordinates). 
     * All the maths requires integers, so to decouple this code from Unity dependence, a custom "IntVector2" class definition is included. If you aren't using
     * Unity, all you need to change is the parameters of FindPath to take what ever type of coordinate system you are using, then convert them to IntVector2 as
     * is being done already for the start and destination vector declarations. Once all the processing is done, convert the final list back to your system as is
     * being done here with the 'result' variable at the end of FindPath.
     */

    static int[,] distanceBuffer;
    static Stack<IntVector2> current, next;
    static int width, height;

    // Points is a 2D bool array with every pixel of the game world, true is allowed movement, false is not
    // As this is an implementation for Unity, the coordinate (0, 0) is in the bottom left of the game world.
    // And for the 2D array, points.GetLength(0) will return width, points.GetLength(1) is height
    public static List<Vector2> FindPath(Vector2 startVector, Vector2 destinationVector, bool[,] points)
    {
        // This conversion is to remove dependence on the Unity Engine Vector2 class, and removes the need for many
        // (int) casts during the calculations
        IntVector2 start = new IntVector2((int)startVector.x, (int)startVector.y);
        IntVector2 destination = new IntVector2((int)destinationVector.x, (int)destinationVector.y);

        // If an invalid location was clicked, get a valid point close to it in line with the player
        destination = GetClosestAllowedPoint(start, destination, points);

        // Allocate these to make later code tidier
        width = points.GetLength(0);
        height = points.GetLength(1);

        InitialiseDistanceBuffer(points);

        // Create the two stacks used for distance calculations
        current = new Stack<IntVector2>();
        next = new Stack<IntVector2>();

        CalculateDistances(start, destination);

        // Get the simplest version of the path, every pixel along the route will have a point in this path
        List<IntVector2> path = TracePath(start);

        if (path == null) return new List<Vector2>(); //If no path is possible, return here

        // Remove each individual point along straight lines and diagonals
        List<IntVector2> simplified = Simplify(path);

        // Smooth out the path where direct lines between points are possible
        List<IntVector2> beautified = Beautify(simplified, points);

        // Convert the final path back into a list of Unity Vector2 (or whatever your implementation warrants)
        List<Vector2> result = new List<Vector2>();
        foreach (var v in beautified)
        {
            result.Add(new Vector2(v.x, v.y));
        }

        return result;
    }

    // If the player has clicked on an invalid movement location, draw a line from the destination to the player
    // and iterate over it from the destination towards the start point and then set the destination as the first valid location
    // found. This will make the player walk somewhat as near to the destination as possible, even if that point itself is invalid
    static IntVector2 GetClosestAllowedPoint(IntVector2 start, IntVector2 destination, bool[,] points)
    {
        List<IntVector2> pointsOnLine = GetPointsOnLine(start, destination);

        //Iterate backwards over the points list, removing the last element if it's invalid until a valid destination point is found
        for (int i = pointsOnLine.Count - 1; i >= 0; i--)
        {
            if (!points[pointsOnLine[i].x, pointsOnLine[i].y])
            {
                pointsOnLine.RemoveAt(i);                
            } else
            {
                return pointsOnLine[i];
            }
        }        

        return start;       
    }


    //Initialise a 2D array of ints the same dimensions as the points array. Set movement allowed points to max integer, 
    // not allowed points to -1
    static void InitialiseDistanceBuffer(bool[,] points)
    {
        distanceBuffer = new int[width, height];

        for(int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (points[x, y])
                {
                    distanceBuffer[x, y] = int.MaxValue;
                } else
                {
                    distanceBuffer[x, y] = -1;
                }
            }
        }
    }

    //Starting from the destination point, iterate outwards, calculating the Manhatten distance (as opposed to the Euclidean distance) for each 
    // point until the start location is reached
    static void CalculateDistances(IntVector2 start, IntVector2 destination)
    {
        current.Push(new IntVector2(destination.x, destination.y));
        distanceBuffer[destination.x, destination.y] = 0; //If this isn't set, destination gets added to 'next' stack (doesn't really matter)

        int dist = 0;
        bool startReached = false;
        while (!startReached)
        {
            /* Next iteration, increase by 1 to get Manhattan distance */
            dist++;

            while (current.Count > 0){
                IntVector2 p = current.Pop();
                int x = p.x;
                int y = p.y;
                                               
                //Check to the left
                if(x > 0 && distanceBuffer[x -1, y] == int.MaxValue) //x > 0 check ensures we don't go out of bounds on the array
                {
                    distanceBuffer[x - 1, y] = dist;
                    next.Push(new IntVector2(x - 1, y));
                }

                //Check to the right
                if(x < width - 1 && distanceBuffer[x + 1, y] == int.MaxValue)
                {
                    distanceBuffer[x + 1, y] = dist;
                    next.Push(new IntVector2(x + 1, y));
                }

                //Check below
                if (y > 0 && distanceBuffer[x, y - 1] == int.MaxValue)
                {
                    distanceBuffer[x, y - 1] = dist;
                    next.Push(new IntVector2(x , y -1));
                }

                //Check above
                if (y < height - 1 && distanceBuffer[x, y + 1] == int.MaxValue)
                {
                    distanceBuffer[x, y + 1] = dist;
                    next.Push(new IntVector2(x, y + 1));
                }

                // Check if we've added a value for the start point, if so, we can stop iterating
                if (start.x == x && start.y == y)
                {
                    startReached = true;
                    break;
                }
            }

            // Nothing added to the next stack? Then we're done 
            if (next.Count == 0)
            {
                break;
            }            

            /* The next stack now becomes the current stack */
            current = new Stack<IntVector2> (next); //Clones the 'next' stack over current - careful about using things like "current = next", it will just change what the variable points to

            next.Clear();

        }       
    }

    static List<IntVector2> TracePath(IntVector2 start)
    {
        // Start dist off as the distance to the start point, we will then follow a trail of decreasing
        // distances in neighbouring pixels until we reach the destination
        int dist = distanceBuffer[start.x, start.y];

        //If the start location never got a distance, it was unreachable
        if (dist == int.MaxValue)
        {
            return null;
        }

        //This list will hold all pixels along the path and ultimately be returned to the game
        List<IntVector2> path = new List<IntVector2>();

        int x = start.x;
        int y = start.y;
        while (true)
        {
            path.Add(new IntVector2(x, y));

            //If reached the destination, distance should be zero
            if (dist == 0) break;

            /* Next distance to look for is one less than the current one */
            dist--;

            /* Allow diagonal movement also, which will have a distance of yet one less */
            int diag = dist - 1;

            /* Check whether the four cardinal directions are allowed */
            bool canGoLeft = (x > 0) && distanceBuffer[x - 1, y] == dist;
            bool canGoRight = (x < width - 1) && distanceBuffer[x + 1, y] == dist;
            bool canGoDown = (y > 0) && distanceBuffer[x, y - 1] == dist;
            bool canGoUp = (y < height - 1) && distanceBuffer[x, y + 1] == dist;
       
            /* And go diagonal if possible */
            if (canGoLeft)
            {
                x--;
                if (canGoUp && distanceBuffer[x, y + 1] == diag)
                {
                    dist = diag;
                    y++;
                }
                else if (canGoDown && distanceBuffer[x, y - 1] == diag)
                {
                    dist = diag;
                    y--;
                }
                continue;
            }
            if (canGoRight)
            {
                x++;
                if (canGoUp && distanceBuffer[x, y + 1] == diag)
                {
                    dist = diag;
                    y++;
                }
                else if (canGoDown && distanceBuffer[x, y - 1] == diag)
                {
                    dist = diag;
                    y--;
                }
                continue;
            }

            /* Only directions remaining to check are straight up and down */
            if (canGoUp)
            {
                y++;
                continue;
            }
            if (canGoDown)
            {
                y--;
                continue;
            }

            /* Never should get here */
            Debug.LogError("No path found (but should be possible)");
            return null;
        }

        return path;
    }

    //Take the list of points from the Find Path method and remove points along straigh runs of lines or diagonals
    static List<IntVector2> Simplify(List<IntVector2> path)
    {
        int currentPoint = 0; //Starting from the start of the list
       
        while (true)
        {
            //Make sure the list is still long enough
            if (currentPoint + 2 >= path.Count) break;

            //Calculate direction between the current point and the one next in the list
            int dx = path[currentPoint + 1].x - path[currentPoint].x;
            int dy = path[currentPoint + 1].y - path[currentPoint].y;

            while (true)
            {
                if (currentPoint + 2 >= path.Count) break; //Check for sufficient list length again

                //If there is a change in direction, we've reached the end of the straight section
                if (path[currentPoint + 2].x - path[currentPoint + 1].x != dx) break;
                if (path[currentPoint + 2].y - path[currentPoint + 1].y != dy) break;

                // Otherwise, remove the middle point. The list will keep shrinking and we continue to look at
                // the new currentPoint + 1 and currentPoint + 2 for a change in direction
                path.RemoveAt(currentPoint + 1);
            }

            //Move the current point to the next point along now that those inbetween have been culled and repeat
            currentPoint++;

        }
        return path;
    }

    static List<IntVector2> Beautify(List<IntVector2> path, bool[,] allowedPoints)
    {
        // Path must be at least 3 points to beautify
        if (path.Count < 3) return path;

        int currentPoint = 0; //Starting from the start of the list

    while (true)
    {
        //Make sure the list is still long enough
        if (currentPoint + 2 >= path.Count) break;

        if (BeelinePossible(path[currentPoint], path[currentPoint + 2], allowedPoints))
        {
            path.RemoveAt(currentPoint + 1);
            continue;
        }

        var p1 = path[currentPoint];
        var p2 = path[currentPoint + 1];
        var p3 = path[currentPoint + 2];

        /* Get the distance between the 2nd and 3rd points */
        int dx = p3.x - p2.x;
        int dy = p3.y - p2.y;
        double dist = Math.Sqrt(dx * dx + dy * dy);

        /* Don't bother with less than 10 pixels */
        if (dist < 10) {
            currentPoint++;
            continue;
        };

        /* Check beelines along at most 10 steps, of at least 5 pixels each */
        double step_size = dist / 10.0;
        int num_steps = 10;
        if (step_size < 5)
        {
            num_steps = (int)(dist / 5);
            if (num_steps < 1) num_steps = 1;
            // step_size = dist / num_steps;
        }

        /* Work backwards, the furthest one found should be it */
        for (int i = num_steps - 1; i > 0; --i)
        {
            IntVector2 p4 = new IntVector2();
            p4.x = p2.x + (dx * i / num_steps);
            p4.y = p2.y + (dy * i / num_steps);
            if (BeelinePossible(p1, p4, allowedPoints))
            {
                /* Move the middle point (where the line segments connect and change direction) to be this location */
                path[currentPoint + 1] = p4;
            }
        }

        currentPoint++;
    }

        return path;
    }

    static bool BeelinePossible(IntVector2 start, IntVector2 destination, bool[,] allowedPoints)
    {
        List<IntVector2> pointsOnLine = GetPointsOnLine(start, destination);

        foreach(var p in pointsOnLine)
        {
            if(!allowedPoints[p.x, p.y])
            {
            return false;
        }
        }

    return true;
    }

    /* Stolen from http://ericw.ca/notes/bresenhams-line-algorithm-in-csharp.html */
    public static List<IntVector2> GetPointsOnLine(IntVector2 start, IntVector2 destination)
    {
        List<IntVector2> result = new List<IntVector2>();

        int x0 = start.x;
        int y0 = start.y;
        int x1 = destination.x;
        int y1 = destination.y;

         bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);
         if (steep)
         {
             int t;
             t = x0; // swap x0 and y0
             x0 = y0;
             y0 = t;
             t = x1; // swap x1 and y1
             x1 = y1;
             y1 = t;
         }

        if (x0 > x1)
        {
            int t;
            t = x0; // swap x0 and x1
            x0 = x1;
            x1 = t;
            t = y0; // swap y0 and y1
            y0 = y1;
            y1 = t;
        }

        int dx = x1 - x0;
        int dy = Math.Abs(y1 - y0);
        int error = dx / 2;
        int ystep = (y0 < y1) ? 1 : -1;
        int y = y0;

        for (int x = x0; x <= x1; x++)
        {
            result.Add(new IntVector2((steep ? y : x), (steep ? x : y)));
            error = error - dy;
            if (error < 0)
            {
                y += ystep;
                error += dx;
            }
        }


        // The original function as copied verbatim above doesn't always return the points in order in the list from start to finish
        // To tell if a list needs to be inverted, draw a line with slope -1 through (0, 0) on a graph. If the point (destination - start)
        // lies below that line, the list needs to be inverted (also a diagonal going down to the right is ok, but up to the left also needs inverting
        // This is just tacked on here for simplicities sake rather than trying to manipulate the function above.

        float xStep = destination.x - start.x;
        float yStep = destination.y - start.y;

        if (yStep <= 0 && xStep < Math.Abs(yStep) || xStep < 0 && Math.Abs(xStep) >= (yStep))
        {
            result.Reverse();
        }

        return result;
    }

    // This simple class is included to decouple this code from using the Unity Vector 2 and simplifies the code,
    // removing many casts to (int)
    public class IntVector2
    {
         public int x, y;

        public IntVector2() { }
        public IntVector2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

}
